<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* room/_form.html.twig */
class __TwigTemplate_0083e84b512a767be626868f437a62d177674219b35f1eed701d6de93e06b5c0 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "room/_form.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "room/_form.html.twig"));

        // line 1
        echo         $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock((isset($context["roomForm"]) || array_key_exists("roomForm", $context) ? $context["roomForm"] : (function () { throw new RuntimeError('Variable "roomForm" does not exist.', 1, $this->source); })()), 'form_start');
        echo "
                ";
        // line 2
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["roomForm"]) || array_key_exists("roomForm", $context) ? $context["roomForm"] : (function () { throw new RuntimeError('Variable "roomForm" does not exist.', 2, $this->source); })()), "name", [], "any", false, false, false, 2), 'row');
        echo "
                ";
        // line 3
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["roomForm"]) || array_key_exists("roomForm", $context) ? $context["roomForm"] : (function () { throw new RuntimeError('Variable "roomForm" does not exist.', 3, $this->source); })()), "capacity", [], "any", false, false, false, 3), 'row');
        echo "
                ";
        // line 4
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["roomForm"]) || array_key_exists("roomForm", $context) ? $context["roomForm"] : (function () { throw new RuntimeError('Variable "roomForm" does not exist.', 4, $this->source); })()), "city", [], "any", false, false, false, 4), 'row');
        echo "
            ";
        // line 5
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["roomForm"]) || array_key_exists("roomForm", $context) ? $context["roomForm"] : (function () { throw new RuntimeError('Variable "roomForm" does not exist.', 5, $this->source); })()), "description", [], "any", false, false, false, 5), 'row');
        echo "
            ";
        // line 6
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["roomForm"]) || array_key_exists("roomForm", $context) ? $context["roomForm"] : (function () { throw new RuntimeError('Variable "roomForm" does not exist.', 6, $this->source); })()), "categories", [], "any", false, false, false, 6), 'row');
        echo "

<button type=\"submit\" class=\"btn btn-primary mt-4\">Valider</button>
";
        // line 9
        echo         $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock((isset($context["roomForm"]) || array_key_exists("roomForm", $context) ? $context["roomForm"] : (function () { throw new RuntimeError('Variable "roomForm" does not exist.', 9, $this->source); })()), 'form_end');
        echo "
";
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "room/_form.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  69 => 9,  63 => 6,  59 => 5,  55 => 4,  51 => 3,  47 => 2,  43 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{{ form_start(roomForm) }}
                {{ form_row(roomForm.name) }}
                {{ form_row(roomForm.capacity) }}
                {{ form_row(roomForm.city) }}
            {{ form_row(roomForm.description) }}
            {{ form_row(roomForm.categories) }}

<button type=\"submit\" class=\"btn btn-primary mt-4\">Valider</button>
{{ form_end(roomForm) }}
", "room/_form.html.twig", "/Users/sophiahmamouche/Documents/Dev/h-sophia-sf4/templates/room/_form.html.twig");
    }
}
