<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* room/index.html.twig */
class __TwigTemplate_5c5a17914d7453a58835e0fa2a20e47328ca4d6f6902edf512ff37c48748e663 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'body' => [$this, 'block_body'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "room/index.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "room/index.html.twig"));

        $this->parent = $this->loadTemplate("base.html.twig", "room/index.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        // line 2
        echo "<div class=\"container\">
  ";
        // line 3
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 3, $this->source); })()), "flashes", [0 => "success"], "method", false, false, false, 3));
        foreach ($context['_seq'] as $context["_key"] => $context["message"]) {
            // line 4
            echo "    <div class=\"alert alert-success\">
      ";
            // line 5
            echo twig_escape_filter($this->env, $context["message"], "html", null, true);
            echo "
    </div>
  ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['message'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 8
        echo "  ";
        if ($this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("IS_AUTHENTICATED_FULLY")) {
            // line 9
            echo "    <a type=\"button\" class=\"btn btn-primary\" href=\"";
            echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("room.add");
            echo "\">Ajouter une salle</a>
  ";
        }
        // line 11
        echo "
  <h1>Salles disponibles (";
        // line 12
        echo twig_escape_filter($this->env, (isset($context["count"]) || array_key_exists("count", $context) ? $context["count"] : (function () { throw new RuntimeError('Variable "count" does not exist.', 12, $this->source); })()), "html", null, true);
        echo "/";
        echo twig_escape_filter($this->env, (isset($context["total"]) || array_key_exists("total", $context) ? $context["total"] : (function () { throw new RuntimeError('Variable "total" does not exist.', 12, $this->source); })()), "html", null, true);
        echo ")</h1>
  <table class=\"table\">
    <thead>
      <tr>
        <th scope=\"col\">#</th>
        <th scope=\"col\">Name</th>
        <th scope=\"col\">City</th>
        <th scope=\"col\">Description</th>
        <th scope=\"col\">Date de création</th>
        <th scope=\"col\">Date de modification</th>
        ";
        // line 22
        if ($this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("IS_AUTHENTICATED_FULLY")) {
            // line 23
            echo "          <th scope=\"col\">Actions</th>
        ";
        }
        // line 25
        echo "      </tr>
    </thead>
    <tbody>
      ";
        // line 28
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["rooms"]) || array_key_exists("rooms", $context) ? $context["rooms"] : (function () { throw new RuntimeError('Variable "rooms" does not exist.', 28, $this->source); })()));
        foreach ($context['_seq'] as $context["_key"] => $context["room"]) {
            // line 29
            echo "      <tr>
        <td>";
            // line 30
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["room"], "id", [], "any", false, false, false, 30), "html", null, true);
            echo "</td>
        <td>";
            // line 31
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["room"], "name", [], "any", false, false, false, 31), "html", null, true);
            echo "</td>
        <td>";
            // line 32
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["room"], "city", [], "any", false, false, false, 32), "html", null, true);
            echo "</td>
        <td>";
            // line 33
            echo twig_escape_filter($this->env, (((1 === twig_compare(twig_length_filter($this->env, twig_get_attribute($this->env, $this->source, $context["room"], "description", [], "any", false, false, false, 33)), 60))) ? ((twig_slice($this->env, twig_get_attribute($this->env, $this->source, $context["room"], "description", [], "any", false, false, false, 33), 0, 60) . "...")) : (twig_get_attribute($this->env, $this->source, $context["room"], "description", [], "any", false, false, false, 33))), "html", null, true);
            echo "</td>
        <td>";
            // line 34
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["room"], "goodFormatTime", [0 => twig_get_attribute($this->env, $this->source, $context["room"], "createdAt", [], "any", false, false, false, 34)], "method", false, false, false, 34), "html", null, true);
            echo "</td>
        <td>";
            // line 35
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["room"], "goodFormatTime", [0 => twig_get_attribute($this->env, $this->source, $context["room"], "updatedAt", [], "any", false, false, false, 35)], "method", false, false, false, 35), "html", null, true);
            echo "</td>
        ";
            // line 36
            if ($this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("IS_AUTHENTICATED_FULLY")) {
                // line 37
                echo "        <td>
          <a href=\"";
                // line 38
                echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("room.edit", ["id" => twig_get_attribute($this->env, $this->source, $context["room"], "id", [], "any", false, false, false, 38)]), "html", null, true);
                echo "\" type=\"button\" class=\"btn btn-warning\">Edit</a>
          <form method=\"post\" action=\"";
                // line 39
                echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("room.delete", ["id" => twig_get_attribute($this->env, $this->source, $context["room"], "id", [], "any", false, false, false, 39)]), "html", null, true);
                echo "\" style=\"display:inline-block\" onsubmit=\"return confirm('Confirmer la suppression ?')\">
            <input type=\"hidden\" name=\"_method\" value=\"DELETE\">
            <input type=\"hidden\" name=\"_token\" value=\"";
                // line 41
                echo twig_escape_filter($this->env, $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderCsrfToken(("delete" . twig_get_attribute($this->env, $this->source, $context["room"], "id", [], "any", false, false, false, 41))), "html", null, true);
                echo "\">
            <button class=\"btn btn-danger\">Delete</button>
          </form>
        </td>
        ";
            }
            // line 46
            echo "      </tr>
      ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['room'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 48
        echo "    </tbody>
  </table>
</div>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "room/index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  184 => 48,  177 => 46,  169 => 41,  164 => 39,  160 => 38,  157 => 37,  155 => 36,  151 => 35,  147 => 34,  143 => 33,  139 => 32,  135 => 31,  131 => 30,  128 => 29,  124 => 28,  119 => 25,  115 => 23,  113 => 22,  98 => 12,  95 => 11,  89 => 9,  86 => 8,  77 => 5,  74 => 4,  70 => 3,  67 => 2,  35 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends 'base.html.twig' %} {% block body %}
<div class=\"container\">
  {% for message in app.flashes('success') %}
    <div class=\"alert alert-success\">
      {{ message }}
    </div>
  {% endfor %}
  {% if is_granted('IS_AUTHENTICATED_FULLY') %}
    <a type=\"button\" class=\"btn btn-primary\" href=\"{{ path('room.add') }}\">Ajouter une salle</a>
  {% endif %}

  <h1>Salles disponibles ({{count}}/{{total}})</h1>
  <table class=\"table\">
    <thead>
      <tr>
        <th scope=\"col\">#</th>
        <th scope=\"col\">Name</th>
        <th scope=\"col\">City</th>
        <th scope=\"col\">Description</th>
        <th scope=\"col\">Date de création</th>
        <th scope=\"col\">Date de modification</th>
        {% if is_granted('IS_AUTHENTICATED_FULLY') %}
          <th scope=\"col\">Actions</th>
        {% endif %}
      </tr>
    </thead>
    <tbody>
      {% for room in rooms %}
      <tr>
        <td>{{ room.id }}</td>
        <td>{{ room.name }}</td>
        <td>{{ room.city }}</td>
        <td>{{ room.description | length > 60 ? room.description | slice(0,60) ~ '...' : room.description}}</td>
        <td>{{ room.goodFormatTime(room.createdAt) }}</td>
        <td>{{ room.goodFormatTime(room.updatedAt) }}</td>
        {% if is_granted('IS_AUTHENTICATED_FULLY') %}
        <td>
          <a href=\"{{ path('room.edit', {id:room.id}) }}\" type=\"button\" class=\"btn btn-warning\">Edit</a>
          <form method=\"post\" action=\"{{ path('room.delete', {id:room.id}) }}\" style=\"display:inline-block\" onsubmit=\"return confirm('Confirmer la suppression ?')\">
            <input type=\"hidden\" name=\"_method\" value=\"DELETE\">
            <input type=\"hidden\" name=\"_token\" value=\"{{ csrf_token('delete'~room.id) }}\">
            <button class=\"btn btn-danger\">Delete</button>
          </form>
        </td>
        {% endif %}
      </tr>
      {% endfor %}
    </tbody>
  </table>
</div>
{% endblock %}
", "room/index.html.twig", "/Users/sophiahmamouche/Documents/Dev/h-sophia-sf4/templates/room/index.html.twig");
    }
}
